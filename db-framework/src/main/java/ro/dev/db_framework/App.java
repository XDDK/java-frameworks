package ro.dev.db_framework;

import ro.dev.dao.spec.impl.DaoActor;
import ro.dev.model.Actor;

/** 
 * 
 * @author Tudor
 *
 *		Class with an example of the DAO implementation
 */
public class App {
	
	private static DaoActor dao;
	
	public static void main(String[] args) {
		dao = new DaoActor();
	
		for(Actor a : dao.findAll()) {
			System.out.println(a);
		}
	}
	
}
