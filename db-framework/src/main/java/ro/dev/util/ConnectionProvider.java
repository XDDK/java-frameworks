package ro.dev.util;

import java.io.InputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;

/** 
 * Provider for the JDBC Connection, using singleton design pattern
 * @author Tudor
 */
public class ConnectionProvider {

	private Connection theConnection = null;
	private static ConnectionProvider theInstance = null;

	private Properties configurationProperties = null;

	private ConnectionProvider() {

	}
	
	/**
	 * @return the provider instance used to get the connection
	 */
	public static ConnectionProvider getInstance() {
		if (theInstance == null) {
			synchronized (ConnectionProvider.class) {
				if (theInstance == null) {
					theInstance = new ConnectionProvider();
				}
			}
		}
		return theInstance;
	}

	/**
	 * Configured for JDBC connection with mysql (properties inside 'connection.properties' file)
	 * @return the connection to the sql database
	 * @throws SQLException
	 */
	public Connection getConnection() throws SQLException {
		if (theConnection == null) {

			if (this.configurationProperties == null) {
				try (InputStream input =  ConnectionProvider.class.getResourceAsStream("/connection.properties");) {
					Properties prop = new Properties();

					prop.load(input);
					this.configurationProperties = prop;

				} catch (Exception e) {
					throw new RuntimeException("FATAL EXCEPTION - could not read connection.properties file properly");
				}
			}

			String dbUsername = this.configurationProperties.getProperty("ro.dev.db.username");
			String dbPassword = this.configurationProperties.getProperty("ro.dev.db.password");
			String dbName = this.configurationProperties.getProperty("ro.dev.db.dbname");
			Integer dbPort = Integer.valueOf(this.configurationProperties.getProperty("ro.dev.db.port"));
			String dbServer = this.configurationProperties.getProperty("ro.dev.db.server");

			String url = "jdbc:mysql://" + dbServer + ":" + dbPort + "/" + dbName;
			
			theConnection = DriverManager.getConnection(url, dbUsername, dbPassword);

		}

		return theConnection;
	}

}
