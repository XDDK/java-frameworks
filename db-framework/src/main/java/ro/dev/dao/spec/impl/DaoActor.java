package ro.dev.dao.spec.impl;

import java.util.Map;

import ro.dev.dao.spec.DaoGeneric;
import ro.dev.dao.spec.DbMap;
import ro.dev.model.Actor;

/**
 * Sample DAO class
 * @author Tudor
 */
public class DaoActor extends DaoGeneric<Actor, Integer>{

	/** 
	 * Constructor which takes a <code>ro.dev.dao.spec.DbMap</code> instance
	 * and builds the pairs of column name - model class field name
	 */
	public DaoActor() {
		super(Actor.class, "actor", "actor_id", new DbMap()
				.addEntry("actor_id", "id")
				.addEntry("first_name", "firstName")
				.addEntry("last_name", "lastName")
				.addEntry("last_update", "lastUpdate"));
	}

	/** 
	 * @param entries constructor which takes a <code>java.util.Map</code> instance, 
	 * each entry representing a pair of column name - model field name
	 */
	public DaoActor(Map<String, String> entries) {
		super(Actor.class, "actor", "actor_id", new DbMap(entries));
	}
	
}
