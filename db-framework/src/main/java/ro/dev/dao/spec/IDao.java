package ro.dev.dao.spec;

import java.util.List;

/**
 * 
 * @author Tudor
 * 
 * Class which specifies the general DAO rules within the framework
 *
 * @param <T> Generic type argument representing the model class
 * @param <PK> Generic type argument representing the Primary Key class (e.g. Integer, String etc.)
 */
public interface IDao<T, PK> {

	/**
	 * @returns An instance of <code>java.util.List</code> of all the entities found in the Database
	 */
	public List<T> findAll();
	
	/**
	 * @param PK - the Primary Key of the entity
	 * @return The Object found by the specific PK
	 */
	public T findById(PK id);
	
	/** 
	 * @param entity - The entity you want to save or update
	 * @returns the entity from the database
	 */
	public T saveOrUpdate(T entity);
	
	/**
	 * @param entity - the entity to save
	 * @return the entity which has been added to the database, including ID if auto incremented
	 */
	public T save(T entity);
	
	/** 
	 * @param entity - The entity you want to save or update
	 * @returns the entity from the database
	 */
	public T update(T entity);
	
	/**
	 * @param id - the Primary Key of the entity to delete
	 */
	public int deleteById(PK id);
	
	/**
	 * @param entity - the Entity to delete
	 */
	public int delete(T entity);

	
}
