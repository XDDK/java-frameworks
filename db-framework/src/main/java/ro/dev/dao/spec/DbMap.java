package ro.dev.dao.spec;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/** 
 * @author Tudor
 * 
 * 			Class which contains the mappings
 * 			between the database columns and the class model of that table
 * 
 */
public class DbMap {

	private List<MappingEntry> daoMappings = null;

	public DbMap() {
		this.daoMappings = new ArrayList<>();
	}
	
	public DbMap(Map<String, String> mappings) {
		this();
		for(Map.Entry<String, String> entry : mappings.entrySet()) {
			this.addEntry(entry.getKey(), entry.getValue());
		}
	}

	public class MappingEntry {

		private final String columnName;
		private final String classFieldName;

		public MappingEntry(String columnName, String classFieldName) {
			this.columnName = columnName;
			this.classFieldName = classFieldName;
		}

		public String getColumnName() {
			return columnName;
		}

		public String getClassFieldName() {
			return classFieldName;
		}

		@Override
		public int hashCode() {
			final int prime = 31;
			int result = 1;
			result = prime * result + ((classFieldName == null) ? 0 : classFieldName.hashCode());
			result = prime * result + ((columnName == null) ? 0 : columnName.hashCode());
			return result;
		}

		@Override
		public boolean equals(Object obj) {
			if (this == obj)
				return true;
			if (obj == null)
				return false;
			if (getClass() != obj.getClass())
				return false;
			MappingEntry other = (MappingEntry) obj;
			if (classFieldName == null) {
				if (other.classFieldName != null)
					return false;
			} else if (!classFieldName.equals(other.classFieldName))
				return false;
			if (columnName == null) {
				if (other.columnName != null)
					return false;
			} else if (!columnName.equals(other.columnName))
				return false;
			return true;
		}

	}

	public List<MappingEntry> entrySet() {
		return this.daoMappings;
	}
	
	/**
	 * @param column - Name of the database table column to be mapped to the field
	 * @param field - Java class field name
	 * @return this
	 */
	public DbMap addEntry(String column, String field) {
		MappingEntry entry = new MappingEntry(column, field);
		if (daoMappings.contains(entry)) {
			throw new RuntimeException("Field  " + column + " already added");
		}
		this.daoMappings.add(entry);
		return this;
	}
	
	public String getColumnName(String fieldName) {
		for(MappingEntry mapping : daoMappings) {
			if(mapping.getClassFieldName().equals(fieldName))
				return mapping.getColumnName();
		}
		return null;
	}
	
	public String getFieldName(String columnName) {
		for(MappingEntry mapping : daoMappings) {
			if(mapping.getColumnName().equals(columnName))
				return mapping.getClassFieldName();
		}
		return null;
	}

}
