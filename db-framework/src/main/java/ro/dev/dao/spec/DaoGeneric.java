package ro.dev.dao.spec;

import java.lang.reflect.Method;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import ro.dev.dao.spec.DbMap.MappingEntry;
import ro.dev.util.ConnectionProvider;

/**
 * @author Tudor
 *
 *         Class which represents an implementation of abstract DAO pattern,
 *         using reflection to map to sql queries such as INSERT / UPDATE
 *
 * @param <T> Generic type argument representing the model class
 * @param <PK> Generic type argument representing the Primary Key class (e.g. Integer, String etc.)
 */
public abstract class DaoGeneric<T, PK> implements IDao<T, PK> {

	private final String tableName;
	private final String pkColumnName;
	private final DbMap dbm;
	private Class<T> clazz;

	/**
	 * @param tableName    - The table name in the database
	 * @param pkColumnName - The column name of the Primary Key
	 */
	public DaoGeneric(Class<T> clazz, String tableName, String pkColumnName, DbMap dbmap) {
		this.tableName = tableName;
		this.pkColumnName = pkColumnName;
		this.dbm = dbmap;
		this.clazz = clazz;
	}
	
	/** 
	 * 
	 * @param rs - ResultSet containing the info of the entity
	 * @return Entity made from the Result Set data
	 */
	protected final T resultSet2ObjectType(ResultSet rs) {
		T entity = null;
		try {
			entity = clazz.newInstance();
			
			for(MappingEntry mappingEntry : dbm.entrySet()) {
				String setterName = mappingEntry.getClassFieldName();
				setterName = "set" + setterName.substring(0, 1).toUpperCase() + setterName.substring(1);
				
				Object objectFromColumn = rs.getObject(mappingEntry.getColumnName());
				for(Method method : entity.getClass().getDeclaredMethods()) {
					// When the setter is found, it's called
					if(method.getName().equals(setterName)) method.invoke(entity, objectFromColumn);
				}
			}
		}catch(Exception e) {
			e.printStackTrace();
			throw new RuntimeException("Mapping error for class: " + clazz);
		}
		return entity;
	};

	/**
	 * @param generatedKeys - ResultSet containing the primary key of the entity
	 *                      (located on the 1st position)
	 * @return The Primary Key
	 * @throws SQLException
	 */
	@SuppressWarnings("unchecked")
	private PK resultSet2ObjectTypePK(ResultSet generatedKeys) throws SQLException {
		return (PK) generatedKeys.getObject(1);
	}

	@Override
	public List<T> findAll() {
		List<T> allEntities = new ArrayList<>();
		try {
			Connection connection = ConnectionProvider.getInstance().getConnection();
			Statement stmt = connection.createStatement();

			ResultSet resultSet = stmt.executeQuery("SELECT * FROM " + this.tableName);
			while (resultSet.next()) {
				T element = resultSet2ObjectType(resultSet);
				allEntities.add(element);
			}
		} catch (Exception e) {
			e.printStackTrace();
			throw new RuntimeException();
		}

		return allEntities;
	}

	@Override
	public T findById(PK id) {
		T foundEntity = null;
		try {
			Connection connection = ConnectionProvider.getInstance().getConnection();

			Statement stmt = connection.createStatement();
			ResultSet resultSet = stmt.executeQuery(
					"SELECT * FROM " + this.tableName + " WHERE " + this.pkColumnName + " = '" + id + "'");

			while (resultSet.next()) {
				foundEntity = resultSet2ObjectType(resultSet);
			}
			stmt.close();
			resultSet.close();
		} catch (Exception e) {
			e.printStackTrace();
			throw new RuntimeException();
		}
		return foundEntity;
	}

	@Override
	public int deleteById(PK id) {
		int affectedRows = 0;

		try {
			Connection connection = ConnectionProvider.getInstance().getConnection();

			String sql = "DELETE FROM " + this.tableName + " WHERE " + this.pkColumnName + " = ?";
			PreparedStatement stmt = connection.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);

			stmt.setObject(1, id);

			affectedRows = stmt.executeUpdate();
			stmt.close();
		} catch (Exception e) {
			e.printStackTrace();
			throw new RuntimeException();
		}
		return affectedRows;
	}

	@Override
	public int delete(T entity) {
		int affectedRows = 0;

		try {
			String sql = "DELETE FROM " + this.tableName + " WHERE " + this.pkColumnName + " = ?";

			Connection connection = ConnectionProvider.getInstance().getConnection();
			PreparedStatement stmt = connection.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);

			String getterName = dbm.getFieldName(this.pkColumnName); // getColumnMappings().get(this.pkColumnName);
			getterName = "get" + getterName.substring(0, 1).toUpperCase() + getterName.substring(1);

			Object PKofEntity = entity.getClass().getMethod(getterName).invoke(entity);

			if (PKofEntity == null)
				throw new SQLException("EXCEPTION - Could not delete the entity, Primary Key value is missing.");

			stmt.setObject(1, PKofEntity);

			affectedRows = stmt.executeUpdate();

			stmt.close();
		} catch (Exception e) {
			e.printStackTrace();
			throw new RuntimeException();
		}

		return affectedRows;
	}

	@SuppressWarnings("unchecked")
	@Override
	public T saveOrUpdate(T entity) {
		PK PKtoWork = null; // when not null, it will update the existing entity

		try {
			Map<String, Object> entityData = new TreeMap<>(); // key - columnName (from DB) ; value - entity data for
																// the respective column (from Java Object)
			for (MappingEntry entry : dbm.entrySet()) {
				String getterMethod = "get" + entry.getClassFieldName().substring(0, 1).toUpperCase()
						+ entry.getClassFieldName().substring(1);
				Object getterValue = entity.getClass().getMethod(getterMethod).invoke(entity);
				if (entry.getColumnName().equals(this.pkColumnName))
					PKtoWork = (PK) getterValue;
				else
					entityData.put(entry.getColumnName(), getterValue);
			}

			StringBuilder sqlBuilder = new StringBuilder();
			if (PKtoWork == null) { // Save entity into the database
				sqlBuilder.append("INSERT INTO " + this.tableName + "(");
				for (int i = 0; i < entityData.size(); i++) {
					if (entityData.values().toArray()[i] != null) { // SAME CONDITION **
						if (i > 0)
							sqlBuilder.append(", ");
						sqlBuilder.append(entityData.keySet().toArray()[i]);
					}
				}
				sqlBuilder.append(") VALUES (");
				for (int i = 0; i < entityData.size(); i++) {
					if (entityData.values().toArray()[i] != null) { // SAME CONDITION **
						if (i > 0)
							sqlBuilder.append(", ");
						sqlBuilder.append("?");
					}
				}
				sqlBuilder.append(")");
			} else { // Update entity on the existing PK
				sqlBuilder.append("UPDATE " + this.tableName + " SET ");
				for (int i = 0; i < entityData.size(); i++) {
					if (entityData.values().toArray()[i] != null) { // SAME CONDITION **
						if (i > 0)
							sqlBuilder.append(", ");
						sqlBuilder.append(entityData.keySet().toArray()[i]);
						sqlBuilder.append(" = ?");
					}
				}
				sqlBuilder.append(" WHERE " + this.pkColumnName + " = ?");
			}

			String sql = sqlBuilder.toString();

			Connection connection = ConnectionProvider.getInstance().getConnection();
			PreparedStatement stmt = connection.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);

			// data for the PreparedStatement

			int i = 0;
			for (Object entityValue : entityData.values()) {
				if (entityValue != null) { // SAME CONDITION **
					i++;
					stmt.setObject(i, entityValue);
				}
			}
			if (PKtoWork != null) {
				stmt.setObject(i + 1, PKtoWork);
			}

			int affectedRows = stmt.executeUpdate();
			if (affectedRows == 0) {
				throw new SQLException("Saving entity failed, no rows affected.");
			}

			try (ResultSet generatedKeys = stmt.getGeneratedKeys()) {
				if (generatedKeys.next()) {
					PKtoWork = resultSet2ObjectTypePK(generatedKeys);
				}
			}

			stmt.close();
		} catch (Exception e) {
			e.printStackTrace();
			throw new RuntimeException("FATAL EXCEPTION - could not Save Or Update the entity");
		}

		return findById(PKtoWork);
	}

	@Override
	public T save(T entity) {
		PK savedPK = null;

		try {
			Map<String, Object> entityValues = new TreeMap<String, Object>();
			for (MappingEntry entry : dbm.entrySet()) {
				String value = entry.getClassFieldName().substring(0, 1).toUpperCase()
						+ entry.getClassFieldName().substring(1);
				Object getterValue = entity.getClass().getMethod("get" + value).invoke(entity);
				if (getterValue != null)
					entityValues.put(entry.getColumnName(), getterValue);
			}

			StringBuilder sql = new StringBuilder("INSERT INTO " + this.tableName);
			sql.append(" (");
			for (int i = 1; i <= entityValues.size(); i++) {
				sql.append(entityValues.keySet().toArray()[i - 1]);
				if (i < entityValues.size())
					sql.append(", ");
			}
			sql.append(")");

			sql.append(" values (");
			for (int i = 1; i <= entityValues.size(); i++) {
				sql.append("?");
				if (i < entityValues.size())
					sql.append(", ");
			}
			sql.append(")");

			String sqlString = sql.toString();

			Connection connection = ConnectionProvider.getInstance().getConnection();
			PreparedStatement stmt = connection.prepareStatement(sqlString, Statement.RETURN_GENERATED_KEYS);

			for (int i = 1; i <= entityValues.size(); i++) {
				stmt.setObject(i, entityValues.values().toArray()[i - 1]);
			}

			int affectedRows = stmt.executeUpdate();

			if (affectedRows == 0) {
				throw new SQLException("Saving entity failed, no rows affected.");
			}

			try (ResultSet generatedKeys = stmt.getGeneratedKeys()) {
				while (generatedKeys.next()) {
					savedPK = resultSet2ObjectTypePK(generatedKeys);
				}
			}

			stmt.close();
		} catch (Exception e) {
			e.printStackTrace();
			throw new RuntimeException();
		}

		return findById(savedPK);
	}

	@SuppressWarnings("unchecked")
	@Override
	public T update(T entity) {
		PK oldPk = null;

		try {
			Map<String, Object> entityValues = new TreeMap<String, Object>();
			for (MappingEntry entry : dbm.entrySet()) {
				String value = entry.getClassFieldName().substring(0, 1).toUpperCase()
						+ entry.getClassFieldName().substring(1);
				Object getterValue = entity.getClass().getMethod("get" + value).invoke(entity);
				if (!entry.getColumnName().equalsIgnoreCase(this.pkColumnName)) {
					if (getterValue != null)
						entityValues.put(entry.getColumnName(), getterValue);
				} else {
					oldPk = (PK) getterValue;
				}
			}
			StringBuilder sql = new StringBuilder("UPDATE " + this.tableName + " SET ");
			for (int i = 1; i <= entityValues.size(); i++) {
				sql.append(entityValues.keySet().toArray()[i - 1]);
				sql.append(" = ?");
				if (i < entityValues.size())
					sql.append(", ");
			}
			sql.append(" WHERE " + this.pkColumnName + " = ?");

			String sqlString = sql.toString();

			Connection connection = ConnectionProvider.getInstance().getConnection();
			PreparedStatement stmt = connection.prepareStatement(sqlString, Statement.RETURN_GENERATED_KEYS);

			for (int i = 1; i <= entityValues.size(); i++) {
				stmt.setObject(i, entityValues.values().toArray()[i - 1]);
			}
			stmt.setObject(entityValues.size() + 1, oldPk);

			int affectedRows = stmt.executeUpdate();

			if (affectedRows == 0) {
				throw new SQLException("Updating entity failed, no rows affected.");
			}

			try (ResultSet generatedKeys = stmt.getGeneratedKeys()) {
				while (generatedKeys.next()) {
					oldPk = resultSet2ObjectTypePK(generatedKeys);
				}
			}

			stmt.close();
		} catch (Exception e) {
			e.printStackTrace();
			throw new RuntimeException();
		}

		return findById(oldPk);
	}

	public String getTableName() {
		return tableName;
	}

	public String getPkColumnName() {
		return pkColumnName;
	}

}
